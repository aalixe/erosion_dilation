#!/usr/bin/env python
import sys
import subprocess
import os
cmd_erodated = 'mkdir ../tests/images_erodated'
os.system(cmd_erodated)

cmd_dilated = 'mkdir ../tests/images_dilated'
os.system(cmd_dilated)


print("tests")

print("First Test Realise whit kernel 3*3")
print("Test  erosion on little image with square kernel 3 * 3")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/White_batman.png",
                                                 "-o","../tests/images_erodated/Image_White_Batman_Square_3.png",
                                                 "-k", "../data/kernels/Kernel_Square3_3.txt",
                                                 "-t","er","-m","CPU"])
print("Test erosion on little image with cirlce kernel 3 * 3")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/White_batman.png",
                                                 "-o","../tests/images_erodated/Image_White_Batman_Circle_3.png",
                                                 "-k", "../data/kernels/Kernel_Circle3_3.txt",
                                                 "-t","er","-m","CPU"])

print("Test  erosion on Medium image with square kernel 7 * 7")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/Medium_White_Point.png",
                                                 "-o","../tests/images_erodated/Medium_White_Point_Square_7.png",
                                                 "-k", "../data/kernels/Kernel_Square7_7.txt",
                                                 "-t","er","-m","CPU"])
print("Test erosion on Medium image with cirlce kernel 7 * 7")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/Medium_White_Point.png",
                                                 "-o","../tests/images_erodated/Medium_White_Point_Circle_7.png",
                                                 "-k", "../data/kernels/Kernel_Circle7_7.txt",
                                                 "-t","er","-m","CPU"])

print("Test  erosion on Large image with square kernel 15 * 15")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/Big_Point_Image.png",
                                                 "-o","../tests/images_erodated/Big_Point_Image_Square_15.png",
                                                 "-k", "../data/kernels/Kernel_Square15_15.txt",
                                                 "-t","er","-m","CPU"])

print("Test erosion on Large image with cirlce kernel 15 * 15")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/Big_Point_Image.png",
                                                 "-o","../tests/images_erodated/Big_Point_Image_Circle_15.png",
                                                 "-k", "../data/kernels/Kernel_Circle15_15.txt",
                                                 "-t","er","-m","CPU"])



print("First Test Realise whit kernel 3*3")
print("Test Dilation on little image with square kernel 3 * 3")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/White_batman.png",
                                                 "-o","../tests/images_dilated/Image_White_Batman_Square_3.png",
                                                 "-k", "../data/kernels/Kernel_Square3_3.txt",
                                                 "-t","di","-m","CPU"])

print("Test Dilation on little image with cirlce kernel 3 * 3")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/White_batman.png",
                                                "-o","../tests/images_dilated/Image_White_Batman_Circle_3.png",
                                                 "-k", "../data/kernels/Kernel_Circle3_3.txt",
                                                 "-t","di","-m","CPU"])

print("Test Dilation on Medium image with square kernel 7 * 7")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/Medium_White_Point.png",
                                                 "-o","../tests/images_dilated/Medium_White_Point_Square_7.png",
                                                 "-k", "../data/kernels/Kernel_Square7_7.txt",
                                                 "-t","di","-m","CPU"])

print("Test Dilation on Medium image with cirlce kernel 7 * 7")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/Medium_White_Point.png",
                                                 "-o","../tests/images_dilated/Medium_White_Point_Circle_7.png",
                                                 "-k", "../data/kernels/Kernel_Circle7_7.txt",
                                                 "-t","di","-m","CPU"])

print("Test Dilation on Large image with square kernel 15 * 15")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/Big_Point_Image.png",
                                                 "-o","../tests/images_dilated/Big_Point_Image_Square_15.png",
                                                 "-k", "../data/kernels/Kernel_Square15_15.txt",
                                                 "-t","di","-m","CPU"])

print("Test erosion on Large image with cirlce kernel 3 * 3")
exec_cpu_erosion_little_image = subprocess.call(['./erosion_dilation',"-i","../data/images/Big_Point_Image.png",
                                                 "-o","../tests/images_dilated/Big_Point_Image_Circle_15.png",
                                                 "-k", "../data/kernels/Kernel_Circle15_15.txt",
                                                 "-t","di","-m","CPU"])
