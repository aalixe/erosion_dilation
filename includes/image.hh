#pragma once

#include <cstddef>
#include <memory>
#include <png.h>
#include <CLI/CLI.hpp>
#include <spdlog/spdlog.h>
#include <iostream>
#include <kernel.hh>
#include <erosion.hh>
#include <dilation.hh>
#include <kernel.hh>

class Image
{
public:
  Image(int width, int height, png_byte color_type,
	png_byte bit_depth, png_structp png_ptr,
	png_infop info_ptr,  int number_of_passes,
	png_bytep* row_pointers,const char* file_name)
  {
    this->width_ = width;
    this->height_ = height;
    this->color_type_ = color_type;
    this->bit_depth_ = bit_depth;
    this->png_ptr_ = png_ptr;
    this->info_ptr_ = info_ptr;
    this->number_of_passes_ = number_of_passes;
    this->row_pointers_ = row_pointers;
    this->file_name_ = file_name;
  }

  void write_png_file(const char* file_name);
  Image* process_file(std::string mode, std::string type, Kernel kernel);

  int width_;
  int height_;
  png_byte color_type_;
  png_byte bit_depth_;
  png_structp png_ptr_;
  png_infop info_ptr_;
  int number_of_passes_;
  png_bytep* row_pointers_;
  const char* file_name_;
};


Image* read_png_file(const char*);
