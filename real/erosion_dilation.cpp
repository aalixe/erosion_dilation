#include <erosion_dilation.hh>
#include <erosion.hh>
#include <dilation.hh>
#include <kernel.hh>
#include <fstream>
#include <string>

Kernel read_kernel_file(const char* file_name)
{
  std::ifstream kernel_file(file_name);
  std::vector<unsigned char> kernel_vector;
  std::string line;
  int width = 0;
  int height = 0;
  if (kernel_file.is_open())
  {
    while(std::getline(kernel_file,line))
    {
      for(unsigned int i= 0; i < line.size(); i++)
      {
        if(int(line[i])-48 >= 0)
        {
          kernel_vector.push_back((unsigned char)line[i]);
          if (height == 0)
            width++;
       }
      }
      height++;
    }
  }
  return Kernel(kernel_vector, width, height-1);
}

void print_kernel(Kernel kernel)
{
  for (int i = 0; i < kernel.height_; i++)
  {
    for (int j = 0; j < kernel.width_; j++)
    {
      std::cout << kernel.kernel_[i*kernel.width_ + j];
    }
    std::cout << std::endl;
  }
}

void print_image(Image* img)
{
    for (int y = 0; y < img->height_; ++y)
	{
	  unsigned char* row = img->row_pointers_[y];
          for (int x = 0; x < img->width_; ++x)
	    {
	      unsigned char* ptr = &(row[x*4]);//shallow copy
              std::cout << int(ptr[0])/255 << " ";
	    }
          std::cout<< std::endl;
	}
}


/*
BENCHMARK

POINT IMG WITH KERNEL:
square 3_3 erosion and dilation
square 7_7 erosion and dilation
cirlce 7_7 erosion and dilation
*/

/*
static void BM_cpu_erosion_point_sq33(benchmark::State& state)
{
  Image* img = read_png_file("../data/images/point.png");
  Kernel kernel = read_kernel_file("../data/kernels/Kernel_Square3_3.txt");
  Image* output = NULL;

  for (auto _ : state)
    {
      output = img->process_file("CPU", "er", kernel);
    }
  
  output->write_png_file("output_cpu_er_point_sq33.png");
  //state.SetComplexityN(state.range(0));
}

BENCHMARK(BM_cpu_erosion_point_sq33)->Complexity();


static void BM_cpu_dilation_point_sq33(benchmark::State& state)
{
  Image* img = read_png_file("../data/images/point.png");
  Kernel kernel = read_kernel_file("../data/kernels/Kernel_Square3_3.txt");
  Image* output = NULL;
  for (auto _ : state)
    {
      output = img->process_file("CPU", "di", kernel);
    }
  output->write_png_file("output_cpu_dila_point_sq33.png");
}

BENCHMARK(BM_cpu_dilation_point_sq33)->Complexity();

static void BM_cpu_erosion_point_sq77(benchmark::State& state)
{
  Image* img = read_png_file("../data/images/point.png");
  Kernel kernel = read_kernel_file("../data/kernels/Kernel_Square7_7.txt");
  Image* output = NULL;

  for (auto _ : state)
    {
      output = img->process_file("CPU", "er", kernel);
    }
  
  output->write_png_file("output_cpu_er_point_sq77.png");
}

BENCHMARK(BM_cpu_erosion_point_sq77)->Complexity();


static void BM_cpu_dilation_point_cir77(benchmark::State& state)
{
  Image* img = read_png_file("../data/images/point.png");
  Kernel kernel = read_kernel_file("../data/kernels/Kernel_Circle7_7.txt");
  Image* output = NULL;
  for (auto _ : state)
    {
      output = img->process_file("CPU", "di", kernel);
    }
  output->write_png_file("output_cpu_dila_point_cir77.png");
}

BENCHMARK(BM_cpu_dilation_point_cir77)->Complexity();

static void BM_cpu_erosion_point_cir77(benchmark::State& state)
{
  Image* img = read_png_file("../data/images/point.png");
  Kernel kernel = read_kernel_file("../data/kernels/Kernel_Circle7_7.txt");
  Image* output = NULL;

  for (auto _ : state)
    {
      output = img->process_file("CPU", "er", kernel);
    }
  
  output->write_png_file("output_cpu_er_point_cir77.png");
}

BENCHMARK(BM_cpu_erosion_point_cir77)->Complexity();


static void BM_cpu_dilation_point_sq77(benchmark::State& state)
{
  Image* img = read_png_file("../data/images/point.png");
  Kernel kernel = read_kernel_file("../data/kernels/Kernel_Square7_7.txt");
  Image* output = NULL;
  for (auto _ : state)
    {
      output = img->process_file("CPU", "di", kernel);
    }
  output->write_png_file("output_cpu_dila_point_sq77.png");
}

BENCHMARK(BM_cpu_dilation_point_sq77)->Complexity();



static void BM_gpu_erosion(benchmark::State& state)
{
  Image* img = read_png_file("../data/images/point.png");

  Kernel kernel = read_kernel_file("../data/kernels/Kernel_Square3_3.txt");

  Image* output = NULL;

  for (auto _ : state)
    {
      std::cout << img;
      output = img->process_file("GPU", "er", kernel);
    }
  output->write_png_file("output_gpu_er_point_33.png");
}

BENCHMARK(BM_gpu_erosion);


static void BM_gpu_dilation(benchmark::State& state)
{
  Image* img = read_png_file("../data/images/point.png");

  Kernel kernel = read_kernel_file("../data/kernels/Kernel_Square3_3.txt");
  Image* output = NULL;
  for (auto _ : state)
    {
      output = img->process_file("GPU", "di", kernel);
    }
  output->write_png_file("output_gpu_dila_point_33.png");
}

BENCHMARK(BM_gpu_dilation);*/


/*
MAIN 

input file          : [-i] finput
output file         : [-o] foutput
structuring element : [-k] se
type                : [-t] type {er, di}
mode                : [-m] mode {GPU, CPU}
*/
int main( int argc, char** argv )
{
  (void) argc;
  (void) argv;

  //Parameters
  std::string finput = "input.png";
  std::string foutput = "output.png";
  std::string se = "se.se";
  std::string type = "er";
  std::string mode = "CPU";
  //Parameters Parsing
  CLI::App app{"morph"};
  app.add_option("-i", finput, "Input image");
  app.add_option("-o", foutput, "Output image");
  app.add_option("-k", se, "Structuring element");
  app.add_option("-t", type, "Morph operation");
  app.add_set("-m", mode, {"GPU", "CPU"}, "Either 'GPU' or 'CPU'");
  CLI11_PARSE(app, argc, argv);

  //Display parameters
  spdlog::info("Runnging {} {} mode with se {} and input image {}.",
	       mode, type, se, finput);


  //Create buffer
  Image* img = read_png_file(finput.c_str());
  Kernel kernel = read_kernel_file(se.c_str());
  //print_kernel(kernel);
  //Call adequate function
  // Save
  Image* output = img->process_file(mode, type, kernel);
  output->write_png_file(foutput.c_str());
  spdlog::info("Output saved in {}.", foutput);

  return 0;
  }

/*
  BENCHMARK MAIN
*/
//BENCHMARK_MAIN();
