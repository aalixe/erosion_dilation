#pragma once

class Morphology
{
 public:
  std::vector<int> erosion_cpu(Kernel& kernel);
  Morphology(){}
  Morphology(std::vector<int>& img,int width, int height)
  {
    this->width_ = width;
    this->height_ = height;
    this->img_ = img;
  }
  int width_=0;
  int height_=0;
  std::vector<int> img_;
};
