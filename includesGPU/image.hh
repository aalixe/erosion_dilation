#pragma once

#include <cstddef>
#include <memory>
#include <png.h>
#include <iostream>
#include <kernel.hh>
#include <erosion.hh>
#include <dilation.hh>

class Image
{
public:
  Image(int width, int height, png_byte color_type,
	png_byte bit_depth, png_structp png_ptr,
	png_infop info_ptr,  int number_of_passes,
	png_bytep* row_pointers)
  {
    this->width_ = width;
    this->height_ = height;
    this->color_type_ = color_type;
    this->bit_depth_ = bit_depth;
    this->png_ptr_ = png_ptr;
    this->info_ptr_ = info_ptr;
    this->number_of_passes_ = number_of_passes;
    this->row_pointers_ = row_pointers;
  }
  Image(const Image* img)
  {
    this->width_ = img->width_;
    this->height_ = img->height_;
    this->color_type_ = img->color_type_;
    this->bit_depth_ = img->bit_depth_;
    this->png_ptr_ = img->png_ptr_;
    this->info_ptr_ = img->info_ptr_;
    this->number_of_passes_ = img->number_of_passes_;
    this->row_pointers_ = (png_bytep *) malloc(sizeof(png_bytep) * this->height_);
    for (int y = 0; y < this->height_; ++y)
      this->row_pointers_[y] = (png_byte *) malloc(png_get_rowbytes(img->png_ptr_,img->info_ptr_));
   }


  void write_png_file(const char* file_name);
  Image* process_file(std::string mode, std::string type, Kernel kernel);

  int width_;
  int height_;
  png_byte color_type_;
  png_byte bit_depth_;
  png_structp png_ptr_;
  png_infop info_ptr_;
  int number_of_passes_;
  png_bytep* row_pointers_;
};


Image* read_png_file(const char*);
