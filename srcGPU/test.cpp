#include <test.hh>
#include <erosion.hh>
#include <dilation.hh>
#include <kernel.hh>
#include <fstream>
#include <string>

Kernel read_kernel_file(const char* file_name)
{
  std::ifstream kernel_file(file_name);
  std::vector<unsigned char> kernel_vector;
  std::string line;
  int width = 0;
  int height = 0;

  if (kernel_file.is_open())
  {
    while(std::getline(kernel_file,line)){
      for(unsigned int i= 0; i < line.size(); i++)
      {
        if(int(line[0])-48 >= 0)
          kernel_vector.push_back((unsigned char)(int(line[0])-48));
      }
    if (width == 0)
      width = kernel_vector.size();
    height++;
  }
  if(width != height){
    std::cout << "width != height in kernel" << std::endl;
    std::exit(1);
  }
  }
  return Kernel(kernel_vector, width, height);
}

int main( int argc, char** argv )
{
  (void) argc;
  (void) argv;

  //Parameters
  std::string finput = "../data/images/point.png";
  std::string foutput = "output.png";
  std::string se = "../data/se21.se";
  std::string type = "er";
  std::string mode = "GPU";

  //Create buffer
  Image* img = read_png_file(finput.c_str());
  Kernel kernel = read_kernel_file(se.c_str());
  //Call adequate function
  if(type == "er")
    gpu_erosion(img, kernel, 0, 0);
  else
    gpu_dilation(img, kernel, 0, 0);
  // Save
  //Image* output = img->process_file(mode, type, kernel);
  img->write_png_file(foutput.c_str());

  return 0;
}
