#include <image.hh>
#include <erosion.hh>
#include <dilation.hh>
#include <pthread.h>



Image* read_png_file(const char* file_name)
{
  char header[8];    // 8 is the maximum size that can be checked

  /* open file and test for it being a png */
  FILE *fp = fopen(file_name, "rb");

  if (!fp)
    return NULL;

  fread(header, 1, 8, fp);

  if (png_sig_cmp(reinterpret_cast<png_const_bytep>(header), 0, 8))
    return NULL;

  /* initialize stuff */
  auto png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (!png_ptr)
    return NULL;

  auto info_ptr = png_create_info_struct(png_ptr);
  
  if (!info_ptr)
    return NULL;

  if (setjmp(png_jmpbuf(png_ptr)))
    return NULL;

  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, 8);

  png_read_info(png_ptr, info_ptr);

  int width = png_get_image_width(png_ptr, info_ptr);
  int height = png_get_image_height(png_ptr, info_ptr);
  
  auto color_type = png_get_color_type(png_ptr, info_ptr);
  auto bit_depth = png_get_bit_depth(png_ptr, info_ptr);

  auto number_of_passes = png_set_interlace_handling(png_ptr);
  png_read_update_info(png_ptr, info_ptr);

  /* read file */
  if (setjmp(png_jmpbuf(png_ptr)))
    return NULL;

  png_bytep* row_pointers = (png_bytep *) malloc(sizeof(png_bytep) * height);
  for (int y = 0; y < height; ++y)
    row_pointers[y] = (png_byte *) malloc(png_get_rowbytes(png_ptr,info_ptr));

  png_read_image(png_ptr, row_pointers);
  fclose(fp);

  Image* img= new Image(width , height, color_type, bit_depth, png_ptr,
			info_ptr, number_of_passes, row_pointers,file_name);
  return img;
}

void Image::write_png_file(const char* file_name)
{
  /* create file */
  FILE *fp = fopen(file_name, "wb");
  if (!fp)
    return;

  /* initialize stuff */
  this->png_ptr_ = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (!this->png_ptr_)
    return;

  this->info_ptr_ = png_create_info_struct(png_ptr_);
  if (!this->info_ptr_)
    return;

  if (setjmp(png_jmpbuf(this->png_ptr_)))
    return;

  png_init_io(this->png_ptr_, fp);

  /* write header */
  if (setjmp(png_jmpbuf(this->png_ptr_)))
    return;

  png_set_IHDR(this->png_ptr_, this->info_ptr_, this->width_, this->height_,
	       this->bit_depth_, this->color_type_, PNG_INTERLACE_NONE,
	       PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  png_write_info(this->png_ptr_, this->info_ptr_);

  /* write bytes */
  if (setjmp(png_jmpbuf(this->png_ptr_)))
    return;

  png_write_image(this->png_ptr_, this->row_pointers_);


  /* end write */
  if (setjmp(png_jmpbuf(this->png_ptr_)))
    return;

  png_write_end(this->png_ptr_, NULL);

  /* cleanup heap allocation */
  /*for (int y = 0; y < height; ++y)
    free(this->row_pointers[y]);
    free(this->row_pointers);*/

  fclose(fp);
}

void update_pixel(png_byte* pixel, png_byte* new_pixel, int pixel_size)
{
  if (new_pixel != NULL)
  {
    std::cout << "update_pixel" << std::endl;
    for(int i = 0; i < pixel_size; i++)
      pixel[i] =  new_pixel[i];
  }
}
Image* Image::process_file(std::string mode, std::string type, Kernel k)
{

  int move_to = 4;
  if (png_get_color_type(this->png_ptr_, this->info_ptr_) == PNG_COLOR_TYPE_RGB)
    move_to = 3;
  /*
    if (png_get_color_type(this->png_ptr_, this->info_ptr_) != PNG_COLOR_TYPE_RGBA)
    {
    std::cout << "RGBQ" << std::endl;
    return this;
    }
  */
  Image* output_image = read_png_file(this->file_name_) ;
  #pragma omp parallel for
  for (int y = 0; y < this->height_; ++y)
  {
    //unsigned char* row = this->row_pointers_[y];
    unsigned char* outrow = output_image->row_pointers_[y];
    #pragma omp parallel for
    for (int x = 0; x < this->width_; ++x)
    {
      //unsigned char* ptr = &(row[x*move_to]);//shallow copy
      //std::cout << int(ptr[0])/255;
      //unsigned char* outptr = &(outrow[x*3]);//shallow copy
      //spdlog::info("Pixel at position [{} - {}] has RGBA values: {} - {} - {} - {}\n",
      //	   x, y, ptr[0], ptr[1], ptr[2], ptr[3]);
      if (mode == "CPU")
      {
        if (type == "er")
          cpu_erosion(this, k, x, y,move_to, &(outrow[x*move_to]));
        else if (type == "di")
           cpu_dilation(this, k, x, y,move_to, &(outrow[x*move_to]));
        //update_pixel(&(outrow[x*move_to]), cpu_dilation(this, k, x, y,move_to),move_to);
      }
    }
  }
  return output_image;
}
