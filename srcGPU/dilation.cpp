#include <dilation.hh>
#include <dilationGPU.hh>
#include <chrono>

#define imax(a,b) (a > b) ? a : b;
#define imin(a,b) (a < b) ? a : b;

using namespace std::chrono;

int cpu_dilation(Image* img,Kernel kernel, int X, int Y)
{
  int startX = X - (img->width_/2);
  int startY = Y - (img->width_/2);
  int return_val = -1;
  for (int i = 0; i < kernel.height_; i++)
  {
    for(int j = 0; j < kernel.width_; j++)
    {
      if(kernel.kernel_[i * 3 + j] == 0 )
        continue;
      int val = 0;
      if((startX + (j * 4) >= 0 && startY + i >= 0) &&
         (startX + (j * 4) < img->width_ && startY + i < img->height_))
      {
        png_byte* row = img->row_pointers_[startY + i];
        unsigned char* pixel = &(row[(startX +j)*4]);
        val = int(pixel[0]);
      }
      if (return_val == -1)
        return_val = val;
      else
        return_val = imax(val, return_val); // dilatation
    }
  }
  return return_val;
}
void gpu_dilation(Image* img, Kernel kernel, int X, int Y)
{
  std::cout << "gpu_dilation" << std::endl;
  unsigned char *ptr = (unsigned char*)malloc(img->width_ * img->height_);
  for(int j = 0; j < img->height_; j++){
    for(int i = 0; i < img->width_; i++){
      ptr[j * img->width_ + i] = (unsigned char)img->row_pointers_[j][i*4];
    }
  }
  auto start = high_resolution_clock::now();
  dilatErode(ptr, img->width_, img->height_, &kernel.kernel_[0], kernel.width_, 1);
  auto stop = high_resolution_clock::now(); 
  auto duration = duration_cast<milliseconds>(stop - start);
  std::cout << "Time taken by function: " << duration.count() << " milliseconds" << std::endl; 
  for(int j = 0; j < img->height_; j++){
    for(int i = 0; i < img->width_; i++){
      img->row_pointers_[j][i*4] = ptr[j * img->width_ + i];
      img->row_pointers_[j][i*4+1] = ptr[j * img->width_ + i];
      img->row_pointers_[j][i*4+2] = ptr[j * img->width_ + i];
    }
  }
  free(ptr);
}
