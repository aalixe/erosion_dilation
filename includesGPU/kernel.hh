#pragma once
#include <vector>
#include <math.h>
#include <string>
class Kernel
{
public:
  Kernel(std::vector<unsigned char>& kernel,int width, int height)
  {
    this->width_ = width;
    this->kernel_ = kernel;
    this->height_ = height;
  }
  // int apply_kernel(std::vector<int> img,int X, int Y, int width, int height);
  int width_;
  int height_;
  std::vector<unsigned char> kernel_;
};
