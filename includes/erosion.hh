#pragma once
#include <kernel.hh>
#include <image.hh>
#include <iostream>

void cpu_erosion(class Image* img, class Kernel& kernel, int& X, int& Y, int& move_to, png_byte* out_pixel);
