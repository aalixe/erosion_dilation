#include <dilation.hh>
//#include <dilationGPU.hh>

#define imax(a,b) (a > b) ? a : b;
#define imin(a,b) (a < b) ? a : b;


void cpu_dilation(Image* img,Kernel& kernel, int& X, int& Y, int& move_to
                 ,png_byte* out_pixel)
{
  int startX = X - (kernel.width_/2);
  int startY = Y - (kernel.height_/2);
  int max_val = -1;
  unsigned char * return_pixel = NULL;
  #pragma omp parallel for
  for (int i = 0; i < kernel.height_; i++)
  {
    #pragma omp parallel for
    for(int j = 0; j < kernel.width_; j++)
    {
      if((int(kernel.kernel_[i * kernel.width_ + j])-48== 0) ||
         ((startX + j < 0) || (startY + i < 0)) ||
         ((startX + j >= img->width_) || (startY + i >= img->height_)))
        continue;

      int val = 0;
      png_byte* row = img->row_pointers_[startY + i];
      unsigned char* pixel = &(row[(startX +j)*move_to]);
      val = (int(pixel[0]) + int(pixel[1]) + int(pixel[2]))/(3*255);

      if (val > max_val)
      {
        max_val = val;
        return_pixel = pixel;
      }
      //return_val = imax(val, return_val); // dilatation
    }
  }
  for(int i = 0; i < move_to; i++)
    out_pixel[i] =  return_pixel[i];
}
