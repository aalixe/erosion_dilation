cmake_minimum_required(VERSION 3.10)

# set the project name
project(erosion_dilation VERSION 1.0)

#FetchContent
include(FetchContent)

FetchContent_Declare(
  GoogleBenchmark
  URL https://github.com/google/benchmark/archive/v1.4.1.tar.gz
)

# Get CLI and SPDLog
FetchContent_Declare(
  CLI11
  URL https://github.com/CLIUtils/CLI11/archive/v1.8.0.tar.gz
)

FetchContent_Declare(
  spdlog
  URL https://github.com/gabime/spdlog/archive/v1.4.2.tar.gz
)

if (NOT GoogleBenchmark_POPULATED)
   FetchContent_Populate(GoogleBenchmark)
   set(BENCHMARK_ENABLE_GTEST_TESTS OFF CACHE BOOL "From Gtest")
   set(BENCHMARK_ENABLE_TESTING OFF CACHE BOOL "From Gtest")
   add_subdirectory(${googlebenchmark_SOURCE_DIR} ${googlebenchmark_BINARY_DIR})
endif()
	
FetchContent_MakeAvailable(spdlog)
FetchContent_MakeAvailable(CLI11)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR})
find_package(PNG REQUIRED)
find_package(CUDA REQUIRED)
find_package(TBB REQUIRED)
find_package(OpenMP REQUIRED)
#find_package(OpenCV REQUIRED)

# Compiler options
add_compile_options(-Wall -Wextra -Wpedantic -g)

# Include directories
include_directories(includes)

# Source files
file(GLOB SOURCES "src/*.cpp")
file(GLOB REAL "real/*.cpp")
file(GLOB BENCH "bench/*.cpp")
file(GLOB OCV "opencv/*.cpp")

# Include files
file(GLOB INCLUDES "includes/*.hh")

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# add the executable erosion_dilation
add_executable(erosion_dilation ${REAL} ${SOURCES} ${INCLUDES})
set_target_properties(erosion_dilation PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
target_link_libraries(erosion_dilation PUBLIC PNG::PNG CLI11::CLI11 spdlog::spdlog tbb OpenMP::OpenMP_CXX)

# add the executable erosion_bench
add_executable(erosion_bench ${BENCH} ${SOURCES} ${INCLUDES})
set_target_properties(erosion_bench PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
target_link_libraries(erosion_bench PUBLIC PNG::PNG CLI11::CLI11 spdlog::spdlog tbb OpenMP::OpenMP_CXX benchmark)

