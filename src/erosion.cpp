#include <erosion.hh>

#define imax(a,b) (a > b) ? a : b;
#define imin(a,b) (a < b) ? a : b;

void cpu_erosion(Image* img, Kernel& kernel, int& X, int& Y, int& move_to
                 ,png_byte* out_pixel)
{
  int startX = X - (kernel.width_/2);
  int startY = Y - (kernel.height_/2);
  int min_val = 255;
  unsigned char* return_pixel = NULL;//new unsigned char[move_to];
  #pragma omp parallel for
  for (int i = 0; i < kernel.height_; i++)
  {
    #pragma omp parallel for
    for(int j = 0; j < kernel.width_; j++)
    {
      if((int(kernel.kernel_[i * kernel.width_ + j])-48== 0) ||
         ((startX + j < 0) || (startY + i < 0)) ||
         ((startX + j >= img->width_) || (startY + i >= img->height_)))
        continue;
      else
      {
        int val = 0;
        png_byte* row = img->row_pointers_[startY + i];
        unsigned char* pixel = &(row[(startX +j) * move_to]);

        val = (int(pixel[0]) + int(pixel[1]) + int(pixel[2]))/(255*3);

        if (min_val == -1)
          min_val = val;
        if (val < min_val)
        {
          min_val = val;
          return_pixel = pixel;
        }
      }
    }
  }
  for(int i = 0; i < move_to; i++)
    out_pixel[i] =  return_pixel[i];
}
