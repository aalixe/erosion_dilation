/*
ptr: pointeur sur l'image
width: longueur de l'image
height: hauteur de l'image
kernel: pointeur sur le kernel
kernelSize: cote du kernel(le kernel doit etre carre)
dilate: booleen, 1=dilatation, 0=erosion
*/
void dilatErode(unsigned char*, int, int, unsigned char *, int, int);
