#include <iostream>
#include <dilationGPU.hh>

void printImg(unsigned char *ptr, int width, int height)
{  
  for(int i = 0; i < height; i++){
    for(int j = 0; j < width; j++){
      std::cout << (int)ptr[i * width + j];
    }
    std::cout << std::endl;
  }
}

void printError(const char *msg)
{
  cudaError_t err = cudaGetLastError();
  std::cout << msg << std::endl;
  std::cout << cudaGetErrorName(err) << " " << cudaGetErrorString(err) << std::endl;
  std::exit(1); 
}

__global__ void dilationKernel(unsigned char *src, unsigned char *dest, int width, int height, unsigned char *kernel, int kernelSize)
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y + threadIdx.y;
  if(x >= width || y >= height)
    return;
  int ker2 = kernelSize / 2;

      unsigned char maxPixel = 0;
      for(int j = 0; j < kernelSize; j++){
        for(int i = 0; i < kernelSize; i++){
          int xx = x + i - ker2;
          int yy = y + j - ker2;
          if(xx < 0 || xx >= width || yy < 0 || yy >= height || kernel[j * kernelSize + i] == 0)
            continue;

          unsigned char pixel = src[yy * width + xx];
          if(pixel > maxPixel)
            maxPixel = pixel;
        }
      }
      dest[y * width + x] = maxPixel;
}

__global__ void erosionKernel(unsigned char *src, unsigned char *dest, int width, int height, unsigned char *kernel, int kernelSize)
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y + threadIdx.y;
  if(x >= width || y >= height)
    return;
  int ker2 = kernelSize / 2;

      unsigned char minPixel = 255;
      for(int j = 0; j < kernelSize; j++){
        for(int i = 0; i < kernelSize; i++){
          int xx = x + i - ker2;
          int yy = y + j - ker2;
          if(xx < 0 || xx >= width || yy < 0 || yy >= height || kernel[j + kernelSize + i] == 0)
            continue;

          unsigned char pixel = src[yy * width + xx];
          if(pixel < minPixel)
            minPixel = pixel;
        }
      }
      dest[y * width + x] = minPixel;
}

void dilatErode(unsigned char *ptr, int width, int height, unsigned char *kernel, int kernelSize, int dilate)
{
  cudaError_t rc = cudaSuccess;
  unsigned char *srcImg;
  unsigned char *resImg;
  unsigned char *devKernel;

  rc = cudaMalloc(&srcImg, width * height * sizeof(unsigned char));
  if(rc)
    printError("Fail buffer allocation");
  rc = cudaMemcpy(srcImg, ptr, width * height * sizeof(unsigned char), cudaMemcpyHostToDevice);
  if(rc)
    printError("Fail buffer copy");

  rc = cudaMalloc(&devKernel, kernelSize * kernelSize * sizeof(unsigned char));
  if(rc)
    printError("Fail buffer allocation");
  rc = cudaMemcpy(devKernel, kernel, kernelSize * kernelSize * sizeof(unsigned char), cudaMemcpyHostToDevice);
  if(rc)
    printError("Fail buffer copy");


  rc = cudaMalloc(&resImg, width * height * sizeof(unsigned char));
  if(rc)
    printError("Fail buffer allocation 2");

  dim3 dimBlock=dim3(16,16);
  int yBlocks=width/dimBlock.y+((width%dimBlock.y)==0?0:1);
  int xBlocks=height/dimBlock.x+((height%dimBlock.x)==0?0:1);
  dim3 dimGrid=dim3(xBlocks,yBlocks);
  if(dilate)
    dilationKernel<<<dimGrid, dimBlock>>>(srcImg, resImg, width, height, devKernel, 3);
  else
    erosionKernel<<<dimGrid, dimBlock>>>(srcImg, resImg, width, height, devKernel, 3);

  rc = cudaMemcpy(ptr, resImg, width * height * sizeof(unsigned char), cudaMemcpyDeviceToHost);
  if(rc)
    printError("Fail buffer copy 2");

  cudaFree(srcImg);
  cudaFree(resImg);
  cudaFree(devKernel);
}

/*int main(void)
{
  unsigned char ptr[] = {0, 0, 1, 1, 1, 0, 0,
                         0, 1, 1, 1, 1, 1, 0,
                         0, 1, 1, 1, 1, 1, 0,
                         0, 1, 1, 1, 1, 0, 0,
                         0, 0, 1, 1, 0, 0, 0};

  unsigned char kernel[] = {1, 1, 1,
                            1, 1, 1,
                            1, 1, 1};
  printImg(ptr, 7, 5);
  std::cout << std::endl << "Dilation:" << std::endl;
  dilatErode(ptr, 7, 5, kernel, 3, 1);
  printImg(ptr, 7, 5);

  return 0;
}*/
