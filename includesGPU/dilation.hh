#pragma once
#include <kernel.hh>
#include <image.hh>
#include <iostream>

int cpu_dilation(class Image* img , class Kernel kernel, int X, int y);
void gpu_dilation(class Image* img, class Kernel kernel, int X, int Y);
