#pragma once
#include <kernel.hh>
#include <image.hh>
#include <iostream>

int cpu_erosion(class Image* img, class Kernel kernel, int X, int Y);
void gpu_erosion(class Image* img, class Kernel kernel, int X, int Y);
